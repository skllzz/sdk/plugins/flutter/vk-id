import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'vk_id_method_channel.dart';

abstract class VkIdPlatform extends PlatformInterface {
  /// Constructs a VkIdPlatform.
  VkIdPlatform() : super(token: _token);

  static final Object _token = Object();

  static VkIdPlatform _instance = MethodChannelVkId();

  /// The default instance of [VkIdPlatform] to use.
  ///
  /// Defaults to [MethodChannelVkId].
  static VkIdPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [VkIdPlatform] when
  /// they register themselves.
  static set instance(VkIdPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<Object?> getAuth();
  Stream<Object?> authChannel();
}
