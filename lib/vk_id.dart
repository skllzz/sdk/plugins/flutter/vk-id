
import 'vk_id_platform_interface.dart';

class VkId {
  Future<Object?> getAuth() {
    return VkIdPlatform.instance.getAuth();
  }
  Stream<Object?> authChannel()  {
    return VkIdPlatform.instance.authChannel();
  }

}
