import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'vk_id_platform_interface.dart';

/// An implementation of [VkIdPlatform] that uses method channels.
class MethodChannelVkId extends VkIdPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final _vkMethodChannel = const MethodChannel('vk_id');
  final _vkAuthChannel = const EventChannel('vk_auth').receiveBroadcastStream();

  @override
  Future<String?> getAuth() async {
    final version = await _vkMethodChannel.invokeMethod<String>('getAuth');
    return version;
  }

  @override
  Stream<Object?> authChannel() {
    return _vkAuthChannel;
  }
}
