import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:vk_id/vk_id.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final vkId = VkId();
  Object? lastAuth;
  StreamSubscription<Object?>? sub;

  @override
  void initState() {
    super.initState();
    sub = vkId.authChannel().listen((event) {
      // setState(() {
      //   lastAuth = event;
      // });
    });
  }

  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Plugin example app'),
            ),
            body: Column(
              children: [
                if (lastAuth != null) Text("$lastAuth"),
                Center(
                    child: ElevatedButton(
                  onPressed: () async {
                    final la=await vkId.getAuth();
                    setState(() {
                      lastAuth = "Auth: $la";
                    });
                  },
                  child: const Text(
                    softWrap: true,
                    "VK",
                  ),
                ))
              ],
            )));
  }
}
