package ru.myfitt.vk_id

import android.util.Log
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.vk.api.sdk.VK
import com.vk.auth.api.models.AuthResult
import com.vk.auth.external.VkExternalAuthManager
import com.vk.auth.main.VkClientAuthCallback
import com.vk.auth.main.VkClientAuthLib
import com.vk.auth.main.VkClientUiInfo
import com.vk.auth.oauth.VkOAuthConnectionResult
import com.vk.auth.ui.fastloginbutton.VkFastLoginButton
import com.vk.superapp.SuperappKit
import com.vk.superapp.SuperappKitConfig
import com.vk.superapp.core.SuperappConfig
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.EventChannel.StreamHandler
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Mutex
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue
import java.util.concurrent.ConcurrentHashMap

private var initLock = Mutex(locked = false)
private var authLock = Mutex(locked = false)

/** VkIdPlugin */
class VkIdPlugin : FlutterPlugin, MethodCallHandler, StreamHandler, ActivityAware {
    private lateinit var pluginScope: CoroutineScope
    private var activity: ActivityPluginBinding? = null

    private lateinit var binding: FlutterPlugin.FlutterPluginBinding
    private lateinit var channel: MethodChannel
    private lateinit var authState: EventChannel
    private var lastAuth: AuthResult? = null
    private var authEvents = ConcurrentHashMap<Any, EventChannel.EventSink?>()
    private lateinit var bq: Channel<String?>
    private val authCallback = object : VkClientAuthCallback {
        override fun onCancel() {
            super.onCancel()
            pluginScope.launch {
                bq.send(null)
                withContext(Dispatchers.Main) {
                    authEvents.forEach {
                        it.value?.apply {
                            success(null)
                        }
                    }
                }
            }
        }

        override fun onOAuthConnectResult(result: VkOAuthConnectionResult) {
            super.onOAuthConnectResult(result)
            if (result is VkOAuthConnectionResult.Error) {
                pluginScope.launch {
                    bq.send(null)
                    withContext(Dispatchers.Main) {
                        authEvents.forEach {
                            it.value?.apply {
                                success(null)
                            }
                        }
                    }
                }

            }
            Log.d("OAuth result", "$result")
        }
        val mapper = ObjectMapper().registerKotlinModule()
        final val json=GsonBuilder().addSerializationExclusionStrategy(object: ExclusionStrategy {
            override fun shouldSkipField(f: FieldAttributes?): Boolean {
                return f?.hasModifier(java.lang.reflect.Modifier.PUBLIC)?:false

            }

            override fun shouldSkipClass(clazz: Class<*>?): Boolean {
                return false
            }


        }).create()
        override fun onAuth(authResult: AuthResult) {
            lastAuth = authResult
            Log.d("Auth", "$authResult")
            val ar =  mapper.writeValueAsString(authResult);
            pluginScope.launch {
                bq.send(ar)
                withContext(Dispatchers.Main) {
                    authEvents.forEach {
                        it.value?.apply {
                            success(ar)
                        }
                    }
                }
            }

        }
    }

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        bq = Channel()
        pluginScope = CoroutineScope(Dispatchers.IO + SupervisorJob())
        binding = flutterPluginBinding
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "vk_id")
        channel.setMethodCallHandler(this)
        authState = EventChannel(flutterPluginBinding.binaryMessenger, "vk_auth")
        authState.setStreamHandler(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        try {
            if (call.method == "getAuth") {
                if (authLock.tryLock()) {
                    activity?.let {
                        pluginScope.launch {
                            val ar = bq.receive()
                            authLock.unlock()
                            withContext(Dispatchers.Main) {
                                result.success(ar)
                            }
                        }
                        try {
                            VkExternalAuthManager(it.activity as FragmentActivity).startExternalAuthFlow()
                        } catch (e: Throwable) {
                            bq.trySend(null)
                        }
                    }
                } else {
                    result.error("IN_PROGRESS", "Auth in progress", null)
                }
            } else {
                result.notImplemented()
            }
        } catch (e: Throwable) {
            result.error("vk_auth_failed", e.message, e)
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        pluginScope.cancel()
        authEvents.clear()
        channel.setMethodCallHandler(null)
        bq.cancel()
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        if (events != null) {
            authEvents[arguments ?: this] = events
            pluginScope.launch {
                withContext(Dispatchers.Main) {
                    lastAuth?.let {
                        events.success(it)
                    }
                }
            }
        }
    }

    override fun onCancel(arguments: Any?) {
        authEvents.remove(arguments ?: this)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding
        if (initLock.tryLock()) {
            val app = binding.activity.application
            val clientSecret = app.resources.getString(R.string.vk_client_secret)
            val appLabel = app.resources.getString(R.string.vk_app_name)
            val defaultIcon = app.applicationInfo.loadIcon(app.packageManager)
            val appInfo = SuperappConfig.AppInfo(
                appLabel,
                VK.getAppId(app).toString(),
                "1.0"
            )
            val config = SuperappKitConfig.Builder(app)
                .setAuthModelData(clientSecret)
                .setAuthUiManagerData(
                    VkClientUiInfo(defaultIcon, appLabel),
                    enablePhoneSelector = true
                )
                .setLegalInfoLinks(
                    serviceUserAgreement = "https://id.vk.com/terms",
                    servicePrivacyPolicy = "https://id.vk.com/privacy"
                )
                .setApplicationInfo(appInfo)
                // Получение Access token напрямую (без silentTokenExchanger)
                .disableMyTrackerAnalytics()
                .setCredentialsManagerEnabled(true)
                .setUseCodeFlow(true).build()
            try {
                SuperappKit.init(config)
            } catch (e: Throwable) {
                Log.e("SuperappKit", "onAttachedToActivity: $e")
                initLock.unlock()
            }
        }
        VkClientAuthLib.addAuthCallback(authCallback)
    }

    override fun onDetachedFromActivityForConfigChanges() {

    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {

    }

    override fun onDetachedFromActivity() {
        VkClientAuthLib.removeAuthCallback(authCallback)
        activity = null
    }
}
